# base docker image for our application
FROM golang:1.16-alpine

# execute commands in this directory
WORKDIR /app

# download go modules
COPY go.mod .
COPY go.sum .
RUN go mod download

# copy .go files
COPY *.go ./

# build executable 
RUN go build -o /docker-compose-demo

# open port
EXPOSE 8080

# run the executable
CMD [ "/docker-compose-demo" ]
